Source: librole-hasmessage-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libmoose-perl <!nocheck>,
                     libmoosex-role-parameterized-perl <!nocheck>,
                     libnamespace-clean-perl <!nocheck>,
                     libstring-errf-perl <!nocheck>,
                     libtest-simple-perl <!nocheck>,
                     libtry-tiny-perl <!nocheck>,
                     perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/librole-hasmessage-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/librole-hasmessage-perl.git
Homepage: https://metacpan.org/release/Role-HasMessage
Rules-Requires-Root: no

Package: librole-hasmessage-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libmoose-perl,
         libmoosex-role-parameterized-perl,
         libnamespace-clean-perl,
         libstring-errf-perl,
         libtry-tiny-perl
Description: Moose roles to summarize the message of an object
 Role::HasMessage is a simple Moose role that provides a method called message
 that returns a string summarizing the message or event represented by an
 object.
 .
 This distribution also provides the Role::HasMessage::Errf module, which is
 an implementation of Role::HasMessage that uses String::Errf to format
 sprintf-like message strings. It adds a message_fmt attribute, initialized by
 the message argument.
